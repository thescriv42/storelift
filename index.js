const express = require('express')
const MyCart = require('./component/ShoppingCart')

const app = express()

app.listen(3000)

app.use('/', (req, res, next) => {
    next ();
})

app.get('/', (req, res) => {
	var product1 = {item: "phone", cost: 200, qty: 2, serialNumber:"3"};
	const carting = new MyCart()

	carting.addProduct(product1);
	carting.addProduct(product1);
	carting.addProduct(product1);
    carting.addProduct( { item: "Briquet", cost: 200, qty: 1, serialNumber: "1" } )

    carting.addProduct( { item: "Briquet", cost: 200, qty: 1, serialNumber: "1" } )

    carting.addProduct( { item: "rubik's", cost: 777, qty: 10, serialNumber: "2" } )

    carting.addProduct( { item: "rubik's", cost: 777, qty: 10, serialNumber: "2" } )

    carting.deleteProduct( { item: "rubik's", cost: 777, qty: 5, serialNumber: "2" } )

    carting.deleteProduct( { item: "Briquet", cost: 200, qty: 1, serialNumber: "1" } )

    carting.getCartInfo()

    carting.getReceipt()

    res.send( {
        'method': 'GET',
        'msg':'everything is fine'
    })
})
