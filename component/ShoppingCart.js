const Table = require('cli-table')
const receipt = require('receipt');

receipt.config.currency = '€ '
receipt.config.width = 60
receipt.config.ruler = '='

const assert = (element, evaluation, errorMessage) => {
    if ([Number, String, Array, Boolean].includes(evaluation) &&
      element.constructor === evaluation) return true
    if (element === evaluation) return true
    throw new Error(errorMessage)
}

class MyCart {
    constructor() {
        this.cart = []
        this.cartPrice = 0
        this.nbArticle = 0
    }
  
    addProduct( product ) {
        assert(!!product.item, true, ".item missing")
        assert(product.item, String, ".item is a String")
        assert(!!product.cost, true, ".cost missing")
        assert(product.cost, Number, ".cost is a Number")
        assert(!!product.qty, true, ".qty missing")
        assert(product.qty, Number, ".qty is a Number")
        assert(!!product.serialNumber, true, ".serialNumber missing")
        assert(product.serialNumber, String, ".serialNumber is a String")
        if (product.cost < 0 || product.qty < 0) {
            throw new Error("addProduct: Cost or Qty can't be negative")
        }
        
        const exist = this.cart.map(function ( elem ) { return elem.serialNumber; }).indexOf(product.serialNumber)
        exist === -1 ? 
			this.cart.push( JSON.parse(JSON.stringify(product)) ) 
				: this.cart[exist].qty += product.qty
        this.cartPrice += (product.cost * product.qty)
        this.nbArticle += product.qty
    }

    deleteProduct( product ) {
        assert(!!product.item, true, ".item missing")
        assert(product.item, String, ".item is a String")
        assert(!!product.cost, true, ".cost missing")
        assert(product.cost, Number, ".cost is a Number")
        assert(!!product.qty, true, ".qty missing")
        assert(product.qty, Number, ".qty is a Number")
        assert(!!product.serialNumber, true, ".serialNumber missing")
        assert(product.serialNumber, String, ".serialNumber is a String")
        if (product.qty <= 0) {
            throw new Error("deleteProduct: qty can't be positive or null")
        }

        const exist = this.cart.map(function ( elem ) { return elem.serialNumber; }).indexOf(product.serialNumber)
        if (exist === -1)
            throw new Error("You can't delete a product that don't exist")
        if (this.cart[exist].qty - product.qty < 1) {
            this.cartPrice -= this.cart[exist].cost * this.cart[exist].qty
            this.nbArticle -= this.cart[exist].qty
            this.cart.splice(exist, 1)
        } else {
            this.cartPrice -= this.cart[exist].cost * product.qty 
            this.nbArticle -= product.qty
            this.cart[exist].qty -= product.qty
        }
    }

    getCartInfo() {
        let cartInfo = new Table({
            head: ['Name', 'Quantity', 'Value']
          , colWidths: [50, 50, 50]
        });
        this.cart.forEach( ( {item, cost, qty} ) => {
            cartInfo.push( [item, qty, (cost * qty) / 100] )
        })
        
        console.log(cartInfo.toString());

        let cartValue = new Table({
            head: ['nbArticle', 'Value']
          , colWidths: [50, 50]
        })
        cartValue.push([this.nbArticle, this.cartPrice / 100])
        console.log(cartValue.toString())
    }
    
    getReceipt() {

        const tva = (this.cartPrice / 100) / 100 * 20
        const cart = this.cart.map( (elem) => {
            return ( {
                item: elem.item,
                qty: elem.qty,
                cost: elem.cost
            } );
        })
        const output = receipt.create([
            { type: 'text', value: [
                'StoreLift',
                'Baie de Hạ Long',
                'Storelift.com'
            ], align: 'center' },
            { type: 'empty' },
            { type: 'properties', lines: [
                { name: 'Date', value: new Date().toGMTString() }
            ] },
            { type: 'table',  lines: cart },
            { type: 'empty' },
            { type: 'text', value: '--Information--', align: 'center' },
            { type: 'empty' },
            { type: 'text', value: `TVA (20.00%) : ${tva.toFixed(2)}`, align:'right'},
            { type: 'text', value: `Total amount (excl. TVA) : ${(this.cartPrice / 100).toFixed(2)}`, align:'right'},
            { type: 'text', value: `Total amount (incl. TVA) : ${(this.cartPrice / 100 + tva).toFixed(2)}`, align:'right'}
        ]);
        console.log(output)
    }

    deleteAll() {
        this.cart = []
        this.cartPrice = 0
        this.nbArticle = 0
    }

  }

  module.exports = MyCart;
