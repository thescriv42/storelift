# StoreLift

Storelift Challenge

>  Créer une library en JS permettant de gérer un panier

git clone le projet

installer le module suivant
* Node.js

installer les packages suivants avec `npm install `
*  express
*  cli-table
*  receipt
*  Nodemon (pour faciliter les tests)

ensuite pour lancer le projet et faire vos tests, lancer la commande `npm start` ou `nodemon index.js`
puis utiliser le logiciel postman pour effectuer une requête `GET` à l'adresse suivante `http://localhost:3000`.

pour commencer vous devez créer un nouveau cart avec
`const cart = new MyCart()`

vous avez un exemple d'utilisation dans le fichier index.js `L:12`

réferez vous plus bas dans la documentation pour avoir plus d'informations.

*******************************************************************************

**Documentation**

dans cette classe se trouve plusieurs fonction :

| Nom           | Description                                      | parametre                     |
| ------ | ------ | ------ |
| addProduct    | Permet d'ajouter un item au panier               | { item, cost, qty, serialNumber } |
| deleteProduct | permet de supprimer un item du panier            | { item, cost, qty, serialNumber } |
| getCartInfo   | permet d'afficher l'eétat du panier en cours     | None                          |
| getReceipt    | permet de recevoir la facture du panier en cours | None                          |
| deleteAll     | permet de vider le panier en cours               | None                          |
 
***********************************************

**Definition parametre**

| Nom | Description |
| ------ | ------ |
| item |  nom du produit voulant être ajouté |
| cost |  prix du produit visé |
| qty  |  nombre de fois que le produit va etre ajouté au panier |
| serialNumber |  numero de serie unique du produit |

***********************************************

**Todo  pour faire avancer le projet**

* Rattacher le projet à une BDD pour faciliter l'état du panier
* Rattacher à une api pour modifier et envoyer l'état du panier plus simplement
* Ajouter des tests unitaires
* Ajouter du front
* Creer une version alternative du package `receipt` pour modifier la presentation du ticket
* Ajouter la prise en charge des promos / discounts
